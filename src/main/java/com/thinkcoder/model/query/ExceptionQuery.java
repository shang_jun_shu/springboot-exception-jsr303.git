package com.thinkcoder.model.query;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.hibernate.validator.constraints.URL;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.*;
import java.util.Date;

@Data
public class ExceptionQuery {

    @NotBlank(message = "用户姓名必须提交")
    @Size(min = 2,max = 8)
    private String userName;

    @NotBlank(message = "用户邮箱不能为空")
    @Email(message = "不合法的邮箱格式")
    private String userEmail;

    @Future(message = "时间必须是当前时间后")
    @Past(message = "时间必须是当前时间前")
    private Date  birthDate;

    @DecimalMax(value ="400",message ="年龄不能大于400")
    @DecimalMin(value = "0",message = "年龄不能小于0")
    private int userAge;

    @Max(value = 400,message = "已超过体重上限")
    @Min(value = 0,message = "体重不能小于0")
    private int userWeight;

    @URL(message = "不正确的url")
    private String userLogo;
}



