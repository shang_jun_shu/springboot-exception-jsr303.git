package com.thinkcoder.model.query;

import com.thinkcoder.jsrtest.First;
import com.thinkcoder.jsrtest.Second;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
public class UserGroupSequence {

    @NotEmpty(message = "id不能为空",groups = First.class)
    private String userId;

    @NotEmpty(message = "姓名不能空",groups = First.class)
    @Size(min = 3,max = 8,message = "姓名长度在3到8之间",groups = Second.class)
    private String userName;
}
