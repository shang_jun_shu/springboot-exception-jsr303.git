package com.thinkcoder.model.query;

import com.thinkcoder.annotation.ListValue;
import lombok.Data;

@Data
public class AnnotationQuery {

    @ListValue(value = {0,1},message = "数值只能是0或者1")
    private Integer isShow;
}
