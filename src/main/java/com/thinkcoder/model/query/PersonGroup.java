package com.thinkcoder.model.query;

import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;

@Data
public class PersonGroup {

    public interface AddGroup{}

    public interface UpdateGroup{}

    @NotBlank(message = "id不能为空",groups = UpdateGroup.class)
    private String personId;

    @NotBlank(message = "名字不能为空",groups = {AddGroup.class,UpdateGroup.class})
    private String personName;

    @Range(min=1,max = 400,message = "年龄提交有误")
    private int personAge;
}
