package com.thinkcoder.model.query;

import lombok.Data;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.List;

@Data
public class People {

    @NotBlank(message = "用户姓名必须提交")
    @Size(min = 2,max = 8)
    private String userName;

    @Valid //能够实现嵌套检测
    @NotEmpty(message = "暂时不能成为秃头")
    private List<Hair> hairs;

}

@Data
class Hair{

    @NotBlank(message = "头发名不能为空字符串")
    private String hairName;

    @NotBlank(message = "颜色描述不能为空字符串")
    private String hairColor;
}

