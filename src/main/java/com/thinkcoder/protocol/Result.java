package com.thinkcoder.protocol;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Result<T> {

    private Boolean success;
    private Integer code;
    private String msg;
    private T data;

    public Result(Boolean success, Integer code, String msg){
        this.success=success;
        this.code=code;
        this.msg=msg;
    }




    /***
     * 构建成功结果
     * @param data
     * @return
     */
    public static Result buildSuccess(Object data){
        return new Result(Boolean.TRUE,200,"返回成功",data);
    }

    /**
     * 构建失败结果
     * @param code
     * @param msg
     * @return
     */
    public static Result buildFailure(Integer code,String msg){
        return new Result(Boolean.FALSE,code,msg);
    }

    public static Result buildFailure(Integer code,String msg,Object data){
        return new Result(Boolean.FALSE,code,msg,data);
    }

}
