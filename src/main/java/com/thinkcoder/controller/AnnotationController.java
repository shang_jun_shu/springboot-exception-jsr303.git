package com.thinkcoder.controller;


import com.thinkcoder.model.query.AnnotationQuery;
import com.thinkcoder.protocol.Result;
import org.hibernate.validator.constraints.Range;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Size;

@RestController
@RequestMapping("/annotation")
@Validated
public class AnnotationController {

    @PostMapping("/add")
    public Result addAnnotation(@Validated @RequestBody AnnotationQuery query){
        return Result.buildSuccess(query);
    }

    @GetMapping("/getname")
    public Result getName(@Size(min = 3,max = 8,message = "姓名长度在3-8之间") @RequestParam String name){
        return Result.buildSuccess(name);
    }

    @GetMapping("/getage")
    public Result getAge(@Range(min = 3,max = 8,message = "年龄在3-8岁") @RequestParam String age){
        return Result.buildSuccess(age);
    }
}
