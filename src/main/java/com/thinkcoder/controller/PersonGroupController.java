package com.thinkcoder.controller;


import com.thinkcoder.model.query.PersonGroup;
import com.thinkcoder.protocol.Result;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/person")
public class PersonGroupController {

    //不指定分组校验
    @PostMapping("/get")
    public Result getPerson(@Validated @RequestBody PersonGroup personGroup){
        return Result.buildSuccess(personGroup);
    }

    //指定AddGroup分组
    @PostMapping("/add")
    public Result addPerson(@Validated(value = PersonGroup.AddGroup.class) @RequestBody PersonGroup personGroup){
        return Result.buildSuccess(personGroup);
    }

    //指定UpdateGroup分组
    @PostMapping("/update")
    public Result updatePerson(@Validated(value = PersonGroup.UpdateGroup.class) @RequestBody PersonGroup personGroup){
        return Result.buildSuccess(personGroup);
    }

}

