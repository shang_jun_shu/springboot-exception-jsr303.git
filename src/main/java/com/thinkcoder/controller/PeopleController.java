package com.thinkcoder.controller;


import com.thinkcoder.model.query.People;
import com.thinkcoder.protocol.Result;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/people")
public class PeopleController {


    @PostMapping("/add")
    public Result getPeople(@Validated @RequestBody People people){
        return Result.buildSuccess(people);
    }
}
