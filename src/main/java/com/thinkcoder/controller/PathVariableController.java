package com.thinkcoder.controller;

import com.thinkcoder.protocol.Result;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PathVariableController {

    @GetMapping("/get/{id:\\d+}")
    public Result getId(@PathVariable(name="id") String userId){
        return Result.buildSuccess(userId);
    }
}
